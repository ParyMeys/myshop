﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MyShop.Domains
{
   public class ProductCategories
    {
        public int Id { get; set; }
        public DateTime CreatedTime { get; set; }
        public DateTime UpdatedTime { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public int? ParentCategoryId { get; set; }
        public ProductCategories ParentCategory { get; set; }
        public List<ProductCategories> Children { get; set; }

        public List<Products>Products { get; set; }
    }
}
