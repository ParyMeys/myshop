﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MyShop.Domains
{
   public class Products
    { 
        public int Id { get; set; }
        public DateTime CreatedTime { get; set; }
        public DateTime UpdatedTime { get; set; }
        public string    Name { get; set; }
        public decimal Price { get; set; }
        public bool Enabled { get; set; }
        public bool ShowOnHomePage { get; set; }
        public int IsSpecial { get; set; }
        public int ProductCategoriesId { get; set; }
        ///
        public ProductCategories ProductCategories { set; get; }
    }
}
