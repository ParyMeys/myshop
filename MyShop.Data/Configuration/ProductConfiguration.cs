﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using MyShop.Domains;
using System;
using System.Collections.Generic;
using System.Text;

namespace MyShop.Data.Configuration
{
    public class ProductConfiguration : IEntityTypeConfiguration<Products>
    {
       
public void Configure(EntityTypeBuilder<Products> builder)
        {
            builder.HasKey(t => t.Id);
            builder.Property(t => t.CreatedTime).IsRequired();
            builder.Property(t => t.Name).IsRequired();
            builder.Property(t => t.Price).IsRequired();
            builder.Property(t => t.ShowOnHomePage).IsRequired();
            builder.HasOne(x => x.ProductCategories)
                .WithMany(x => x.Products)
                    .HasForeignKey(x => x.ProductCategoriesId);
        }
       
    }
    }
