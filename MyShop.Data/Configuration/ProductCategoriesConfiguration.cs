﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using MyShop.Domains;
using System;
using System.Collections.Generic;
using System.Text;

namespace MyShop.Data.Configuration
{
    public class ProductCategoriesConfiguration : IEntityTypeConfiguration<ProductCategories>
    {
        public void Configure(EntityTypeBuilder<ProductCategories> builder)
        {
            builder.HasOne(x => x.ParentCategory)
                        .WithMany(x => x.Children)
                        .HasForeignKey(x => x.ParentCategoryId);
            builder.HasKey(x => x.Id);
            builder.Property(x => x.Id).IsRequired().UseSqlServerIdentityColumn();

               
                
        }
    }
}
