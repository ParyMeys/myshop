﻿using Microsoft.EntityFrameworkCore;
using MyShop.Data.Configuration;
using MyShop.Domains;
using System;
using System.Collections.Generic;
using System.Text;

namespace MyShop.Data
{
   public class MyshopContext :DbContext
    {
        public MyshopContext(DbContextOptions<MyshopContext> options) : base(options)
        {
        }

        public DbSet<Products> Products { get; set; }

        public DbSet<ProductCategories> ProductCategories { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfiguration(new ProductCategoriesConfiguration());
            modelBuilder.ApplyConfiguration(new ProductConfiguration());
            modelBuilder.Entity<Products>().ToTable("Products");
            modelBuilder.Entity<ProductCategories>().ToTable("ProductCategories");

        }

    }
}
